__version__ = '0.6.3'
__author__ = '9ulo9ulo'


import bpy
from mathutils import Vector


class DataVisualiser3d(object):


    init_y = -13.8              #how to calculate this automagically?
    offset_y = 0.2
    HEIGHT_RATIO = 0.1


    def __init__(self, meas_dict, bar_shape='CUBE', DEL_DUPLICATES=1):
        self.bar_chart = []
        self.tip_coords = []
        self.meas_dict = meas_dict
        self.bar_shape = bar_shape
        self.DEL_DUPLICATES = DEL_DUPLICATES
        self._print_all_meshes()
        if self.DEL_DUPLICATES:
            self.del_duplicated_meshes()


    def _print_all_meshes(self):
        '''
        this method prints all meshes (mesh names) found on the current scene
        '''
        print('already defined meshes:')
        for mesh in bpy.data.meshes:
            print('found mesh: "{}"'.format(mesh.name))
        print('total: {}'.format(len(bpy.data.meshes)))


    def del_duplicated_meshes(self):
        ground_plane_mesh_name = 'ground_plane_mesh'
        chart_bar_mesh_name_list = ['h'+str(i).zfill(2)+'_mesh' for i in range(24)]
        chart_bar_tip_mesh_name_list = ['h'+str(i).zfill(2)+'_tip_mesh' for i in range(24)]
        mesh_names = [ground_plane_mesh_name] + chart_bar_mesh_name_list + chart_bar_tip_mesh_name_list
        for mesh in bpy.data.meshes:
            #deleting duplicated meshes for ground plane, chart bar and chart bar tips
            mesh_name = mesh.name
            if mesh_name in mesh_names:
                print('deleting mesh: "{}"'.format(mesh_name))
                bpy.data.meshes.remove(mesh)
                print('deleted mesh: "{}"'.format(mesh_name))
            else:
                print('this mesh will be not removed: {}'.format(mesh_name))


    def set_grid_lines(self):
        '''
        this method configures grid lines (ground plane) on 3D view
        '''
        for area in bpy.context.screen.areas:
            if area.type == 'VIEW_3D':
                area.spaces[0].grid_lines = 32
                area.spaces[0].grid_subdivisions = 4


    def _make_material(self, mat_name, diffuse_color, specular_color, diffuse_intensity=0.8, specular_intensity=0.5, emit=0.0, ambient=1.0, alpha=1.0):
        mat = bpy.data.materials.new(mat_name)
        mat.diffuse_color       = diffuse_color
        mat.diffuse_shader      = 'LAMBERT'
        mat.diffuse_intensity   = diffuse_intensity
        mat.specular_color      = specular_color
        mat.specular_shader     = 'COOKTORR'
        mat.specular_intensity  = specular_intensity
        mat.emit                = emit
        mat.ambient             = ambient
        mat.alpha               = alpha
        return mat


    def create_ground_plane(self):
        '''
        this method creates ground plane object. the size is: x=5 blender
        units, y=32 blender units
        '''
        bpy.ops.mesh.primitive_plane_add(location=(1, 0, 0.01))
        ob = bpy.context.object
        ob.name = 'ground_plane'
        ob.dimensions = (5, 32, 0)
        bpy.ops.object.transform_apply(scale=True)
        ob.scale = (1.0, 1.0, 1.0)
        ob.data.name = ob.name + '_mesh'
        mat_blue = self._make_material(ob.name + '_material',
                                       diffuse_color=(0, 0.355, 0.815),         #blue
                                       diffuse_intensity=0.5,
                                       specular_color=(1, 1, 1),
                                       specular_intensity=0.9)
        ob.data.materials.append(mat_blue)


    def create_bar(self, bar_name, pos, h):
        size_x = 1
        size_y = 1
        if self.bar_shape == 'CUBE':
            bpy.ops.mesh.primitive_cube_add(location=pos)
        elif self.bar_shape == 'CYLINDER':
            bpy.ops.mesh.primitive_cylinder_add(vertices=64, location=pos)
        else:
            print('error! unsupported value for bar_shape: {}'.format(self.bar_shape))
        ob = bpy.context.object
        bar_obj = ob
        ob.name = bar_name
        ob.show_name = True
        ob.dimensions = (size_x, size_y, h)
        bpy.ops.object.transform_apply(scale=True)
        ob.scale = (1.0, 1.0, 1.0)
        mesh_name = ob.name + '_mesh'
        print('mesh_name = {}'.format(mesh_name))
        ob.data.name = mesh_name
        #creating and adding material
        mat_purple = self._make_material(ob.name+'_material',
                                         diffuse_color=(0.644, 0.094, 0.8),     #purple
                                         diffuse_intensity=0.5,
                                         specular_color=(1, 1, 1),
                                         specular_intensity=0.9)
        ob.data.materials.append(mat_purple)
        #create tip: small yellow sphere on top of the bar
        TIP_SIZE = 0.1
        bpy.ops.mesh.primitive_uv_sphere_add(segments=32,
                                             ring_count=16,
                                             size=TIP_SIZE,
                                             location=(0, 0, (h/2.0)+TIP_SIZE))
        #update self.tip_coords list
        self.tip_coords.append(Vector((0.75, pos[1], h+TIP_SIZE)))
        ob = bpy.context.object
        tip_name = bar_name + '_tip'
        ob.name = tip_name
        ob.data.name = tip_name + '_mesh'
        mat_yellow = self._make_material(tip_name+'_material',
                                         diffuse_color=(0.795, 0.8, 0),         #yellow
                                         diffuse_intensity=1,
                                         specular_color=(1, 1, 1),
                                         specular_intensity=0.75)
        ob.data.materials.append(mat_yellow)
        ob.parent = bar_obj
        ob.select = False
        return bar_obj


    def create_trend_line(self):
        obj_name = 'trend_line'
        weight = 1
        curve_data = bpy.data.curves.new(name=obj_name+'_curve', type='CURVE')
        curve_data.dimensions = '3D'
        curve_data.extrude = 0
        curve_data.fill_mode = 'FULL'
        curve_data.bevel_depth = 0.04
        curve_data.bevel_resolution = 4
        object_data = bpy.data.objects.new(obj_name, curve_data)
        bpy.context.scene.objects.link(object_data)
        poly_line = curve_data.splines.new('NURBS')
        poly_line.points.add(len(self.tip_coords) - 1)
        for num in range(len(self.tip_coords)):
            x, y, z = self.tip_coords[num]
            poly_line.points[num].co = (x, y, z, weight)
        poly_line.order_u = 2
        poly_line.resolution_u = 64
        mat_green = self._make_material(obj_name+'_curve_material',
                                         diffuse_color=(0.09, 0.8, 0),         #light green
                                         specular_color=(1, 1, 1),
                                         emit=0.35)
        object_data.data.materials.append(mat_green)


    def create_chart(self):
        self.set_grid_lines()
        self.create_ground_plane()
        for y in range(24):
            bar_h = (self.meas_dict[y] * self.HEIGHT_RATIO) + 0.05
            bar_pos_y = self.init_y + y + (self.offset_y * y)
            bar_pos_z = 0.02 + (bar_h / 2.0)
            self.bar_chart.append(self.create_bar(bar_name='h'+str(y).zfill(2),
                                              pos=(0, bar_pos_y, bar_pos_z),
                                              h=bar_h))
        self.create_trend_line()




#main function
def main(meas_dict, bar_shape='CUBE'):
    print('measurement data:')
    print('meas_dict = {}'.format(meas_dict))
    dv = DataVisualiser3d(meas_dict, bar_shape)
    dv.create_chart()
    print('chart = {}'.format(dv.bar_chart))




#call context
if __name__ == '__main__':
    #dict with 24 int keys: 0, 1, ..., 23
    example_data_dict = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 19, 8: 10, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 46, 15: 5, 16: 0, 17: 39, 18: 14, 19: 0, 20: 0, 21: 0, 22: 0, 23: 0}
    bar_shape = 'CUBE'          #default
#    bar_shape = 'CYLINDER'
    main(meas_dict=example_data_dict, bar_shape=bar_shape)
    print('done')




#70D0:
# - convert to pure addon! ;)
# - add missing docstrings
# - use logging module here
# - add labels for all values
# - allow loading data from external file ;)
# - animate it (optionally)
# - handle camera and light sources for rendering
# - prepare for batch mode (perhaps argparse module will ne needed)
# - use bevel for smooth modeling
# - input data normalization is needed :(
# - vertical axis (scale) should be added
# - horizontal lines (scale) should be added
# - add deleting materials method
# - too many values are hardcoded in equations :( refactoring is needed
# - smart method for generating and managing materials is needed

